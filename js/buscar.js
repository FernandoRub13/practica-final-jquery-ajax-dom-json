$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);
		
		$.ajax({
			url: `http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query=${palabra													}`,
			method: 'GET',
			dataType: 'json',
			success: function(data){
				console.log(data);
				var peliculas = data.results;
				console.log(peliculas);
				var html = '';
				if (peliculas.leght == 0) {
					html += '<li>No hay resultados</li>';
				}else{
					for(var i = 0; i < peliculas.length; i++){
						html += 
						'<div class="col-md-4">'
		    +'<div class="card">'
		       +'<div class="card-header">'
		          +'<img class="card-image" src="https://image.tmdb.org/t/p/w500/'+peliculas[i].poster_path+'" alt="peliculas[i].original_title">'
		       +'</div>'
		       +'<div class="card-body">'
		          +'<h2 class="card-title">'+peliculas[i].original_title+'</h2>'
		          +'<div class="container">'
		             +'<div class="row">'
		                +'<div class="col-4 metadata">'
		                   +'<i class="fa fa-star" aria-hidden="true"></i>'
		                   +'<p>9.5/10</p>'
		                +'</div>'
		                +'<div class="col-8 metadata">Adventure. Sci-Fi</div>'
		             +'</div>'
		          +'</div>'
		          +'<p class="card-text">'+cutString(peliculas[i].overview,75)+'</p>'
		       +'</div>'
		    +'</div>'
		+'</div>'
		+'<!-- CARD -->';
					}
								
				}
				miLista.html(html);
			},
			error: function(error){
				console.log(error);
			}
		});
		
			
		
	});
});
// Function that cut a string in a specific length
function cutString(string, length){
	if (string.length > length) {
		string = string.substring(0, length) + "...";
	}
	return string;
}

